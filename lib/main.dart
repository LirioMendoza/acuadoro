
import 'package:flutter/material.dart';
import 'package:acuadoro/goals_page.dart';
import 'package:acuadoro/aquadoro.dart';
import 'package:acuadoro/pantalla_carga.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
     
      title: 'Material App',
      debugShowCheckedModeBanner: false,
      routes: {
        'pantallaCarga' : (BuildContext context) => PantallaDeCarga(),
        'goalsPage' : (BuildContext context) => GoalsPage(),
        'aquadoro' : (BuildContext context) => Aquadoro(),
      },
      initialRoute: 'pantallaCarga',
    );
  }
}